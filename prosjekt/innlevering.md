# Innlevering av inf112-prosjekt

Det er tre viktige steg:

* Dere må gi gruppelederne tilgang til prosjektet
* Dere må tagge committen som er «innleveringen»
* Send en melding til gruppeleder med lenke til prosjektet

Pass ellers på at tekst-delen av innleveringen er med og i Markdown-format (`Deliverables/ObligatoriskOppgave1.md`), og at den inneholder navnet på alle team-medlemmene og hvilken gruppe dere er på.

## Gi tilgang til prosjekt

Dette må gjøres i [GitLab](https://git.app.uib.no/).

På prosjektsiden, gå til *Members*, klikk på *Invite Group*, og inviter *INF112 / 22V*. Sett *access level* til *Maintainer*, og trykk *Invite*:

![skjermbilde](https://git.app.uib.no/inf112/22v/lectures/-/raw/master/img/invite-to-project.png)

Hvis dere har valgt en Git-arbeidsflyt dere hver av dere har deres egen fork av prosjektet, pass på å gi tilgang til «hovedprosjektet», og pass på at alle team-medlemmene har merget inn sine endringer.

## Tag innleverings-commit

*Først:* pass på at alt er pushet til GitLab og dere har integrert alle endringene dere vil ha med fra alle team-medlemmene.


For å lage ny tag i GitLab, trykk på `Tags` på linjen under overskriften på prosjektforsiden:

![skjermbilde](https://git.app.uib.no/inf112/22v/lectures/-/raw/master/img/tags.png)

På neste side, velg *New Tag*, og fyll så inn informasjonen:

![skjermbilde](https://git.app.uib.no/inf112/22v/lectures/-/raw/master/img/tag-release.png)

Det går også an å tagge fra kommandolinjen (push i såfall etterpå, og se at taggen er synlig i GitLab) med f.eks. `git tag -a v1.4 -m 'version 1.4'`. Pass på at du står på `master`-grenen og er oppdatert med alle endringer fra team-medlemmene.

